#include "Arduino.h"
#include "SoftwareSerial.h"
#include "DFPlayerMini_Fast.h"
#include "StateMachine.h"
#include "JC_Button.h"          // https://github.com/JChristensen/JC_Button


/********************** DEFINES **********************/
#define dialSwPin 2
#define dialStatSwPin 3
#define handsetSwPin 4

#define dfpRX 10
#define dfpTX 11

#define volPot A0

#define debounceTime 10

/*********************** OBJECTS ***********************/
SoftwareSerial mySoftwareSerial(dfpRX, dfpTX); // RX, TX
DFPlayerMini_Fast myMP3;

StateMachine machine = StateMachine();

Button dialSw(dialSwPin, 2);
Button dialStatSw(dialStatSwPin, 2);
Button handsetSw(handsetSwPin, 2);


/********************** FUNCTIONS **********************/
void phoneInit();
void dial();
void playSound();

bool transitionInitDial();
bool transitionDialInit();
bool transitionDialPlaySound();
bool transitionPlaySoundInit();
bool transitionPlaySoundDial();

/************************ STATES ************************/
State* ini = machine.addState(&phoneInit);
State* dia = machine.addState(&dial);
State* plaS = machine.addState(&playSound);


/********************** VARIABLES **********************/
uint32_t dialNum = 0;
uint32_t expNum = 0;
uint32_t songToPlay = 0;

uint32_t dialExp[] = {1000, 100, 10, 1};


void setup() {
    Serial.begin(115200);
    mySoftwareSerial.begin(9600);
    /*
    if (!myDFPlayer.begin(mySoftwareSerial)) {  //Use softwareSerial to communicate with mp3.
        Serial.println(F("Unable to begin:"));
        Serial.println(F("1.Please recheck the connection!"));
        Serial.println(F("2.Please insert the SD card!"));
        while(true);
    }*/
    myMP3.begin(mySoftwareSerial);

    Serial.println(F("DFPlayer Mini online."));

    dialSw.begin();
    dialStatSw.begin();
    handsetSw.begin();


    /********************** TRANSITIONS *********************/
    ini->addTransition(&transitionInitDial,dia);    // Transition to itself (see transition logic for details)
    dia->addTransition(&transitionDialInit,ini);  // S2 transition to S3
    dia->addTransition(&transitionDialPlaySound,plaS);  // S1 transition to S2
    plaS->addTransition(&transitionPlaySoundInit,ini);  // S3 transition to S4
    plaS->addTransition(&transitionPlaySoundDial,dia);  // S4 transition to S5

}


void loop() {
    int val = analogRead(volPot);
    val = map(val, 0, 1023, 0, 30);
    myMP3.volume(val);

    dialSw.read();
    dialStatSw.read();
    handsetSw.read();

    machine.run();
}

void phoneInit(){
    if(machine.executeOnce){
        Serial.println("init");
        if (myMP3.trackIsPlaying() == 1) {
            myMP3.pause();
        }
    }

}

bool transitionInitDial(){
    //return digitalRead(handsetSw) == false;
    return handsetSw.isPressed();
}

void dial(){
    if(machine.executeOnce){
        Serial.println("dial");
        dialNum = 0;
        expNum = 1;
    }

    if (dialSw.wasReleased()) {
        dialNum++;


    }
    if (dialStatSw.wasReleased()) {

        if (dialNum >= 10) {
            dialNum = 0;
        }
        songToPlay = songToPlay + dialNum * dialExp[expNum];
        Serial.println(songToPlay);
        expNum++;
        dialNum = 0;

    }

}

bool transitionDialInit(){
    return handsetSw.isReleased();
}

bool transitionDialPlaySound(){
    return expNum >= 4;
}

void playSound(){
    myMP3.play(songToPlay);

    Serial.println("playSound");
    Serial.println(songToPlay);

}


bool transitionPlaySoundInit(){
   return handsetSw.isReleased();

}

bool transitionPlaySoundDial(){
    return true;
}
